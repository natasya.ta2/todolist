import com.bncc.Task;
import com.bncc.TodoList;
import org.junit.Test;

import static org.junit.Assert.*;

public class TodoListTest {

    @Test
    public void testAddOneTodo(){

        String expectedTodo = "1. Do dishes [DONE]";

        TodoList todoList = new TodoList();
        Task todo1 = new Task (1, "Do dishes", "DONE");
        todoList.addTodo(todo1);

        assertEquals(expectedTodo, todoList.getTodo(0));

    }

   @Test
    public void testAddMultipleTodo(){
       String expectedTodo = "1. Do dishes [DONE]\n2. Learn Java [NOT DONE]\n3. Learn TDD [NOT DONE]";

       TodoList todoList = new TodoList();
       Task todo1 = new Task (1, "Do dishes", "DONE");
       Task todo2 = new Task (2, "Learn Java", "NOT DONE");
       Task todo3 = new Task (3, "Learn TDD", "NOT DONE");
       todoList.addTodo(todo1);
       todoList.addTodo(todo2);
       todoList.addTodo(todo3);

       assertEquals(expectedTodo, todoList.getAllTodo());
    }

}