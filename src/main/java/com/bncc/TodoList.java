package com.bncc;

import java.util.ArrayList;


public class TodoList  {
    private ArrayList<Task> todos;

    public TodoList(){
       this.todos = new ArrayList<Task>();
    }

    public void addTodo (Task newTodo){
        todos.add(newTodo);
    }

    public String getTodo(int index) {
        return todos.get(index).showTodo();
    }


    public String getAllTodo(){
        String temp ="";
        for (int i=0; i<todos.size(); i++){
            if(i<todos.size()-1){
                temp += todos.get(i).showTodo()+"\n";
            }
            else{
                temp+= todos.get(i).showTodo();
            }
        }
        return temp;
    }

}
