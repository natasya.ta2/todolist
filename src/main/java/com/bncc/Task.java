package com.bncc;

public class Task {
    int id;
    String name;
    String status;

    public Task (int id, String name, String status){
        this.id = id;
        this.name = name;
        this.status = status;
    }

    public String showTodo(){
        return this.id+". "+this.name+" ["+ this.status +"]";
    }

}
